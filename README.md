# Notepad-- Helloworld 插件

- 在线构建支持
    ```cmake
    # 1. 查找 NotepadPlugin 模块
    find_package(NotepadPlugin REQUIRED)

    # 2. 基于 git 仓库在线构建 helloworld 插件（online-helloworld）
    add_notepad_plugin_with_git(online-helloworld
        https://gitee.com/zinface/notepad--plugin.helloworld helloworld)

    # 3. 没有依赖
    # find_package(Qt5Core)
    # target_link_libraries(online-helloworld Qt5::Core)
    ```

- 效果图

    ![](images/20230210201747.png)  