#include "helloworld.h"
#include "ui_helloworld.h"

#include <qsciscintilla.h>

HelloWorld::HelloWorld(QWidget *parent, QsciScintilla *pEgit) :
    QMainWindow(parent),
    ui(new Ui::HelloWorld),
    m_pEdit(pEgit)
{
    ui->setupUi(this);
}

HelloWorld::~HelloWorld()
{
    delete ui;
}

void HelloWorld::on_pushButton_clicked()
{
    m_pEdit->setText(m_pEdit->text().toUpper());
}

void HelloWorld::on_pushButton_2_clicked()
{
    m_pEdit->setText(m_pEdit->text().toLower());
}
