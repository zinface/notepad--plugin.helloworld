#ifndef HELLOWORLD_H
#define HELLOWORLD_H

#include <QMainWindow>

namespace Ui {
class HelloWorld;
}

class QsciScintilla;
class HelloWorld : public QMainWindow
{
    Q_OBJECT

public:
    explicit HelloWorld(QWidget *parent, QsciScintilla *pEgit);
    ~HelloWorld();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::HelloWorld *ui;
    QsciScintilla *m_pEdit;
};

#endif // HELLOWORLD_H
