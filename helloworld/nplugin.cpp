#include "helloworld.h"

#include <qobject.h>
#include <qstring.h>
#include <pluginGl.h>
#include <functional>
#include <qsciscintilla.h>


bool NDD_PROC_IDENTIFY(NDD_PROC_DATA* pProcData)
{
    NOTEPAD_PLUGIN_METADATA("HelloWorld", "v0.1", "zinface", "全文大小写转换", "");
    return true;
}

// 插件的入口点函数
int NDD_PROC_MAIN(QWidget* pNotepad, const QString &strFileName, std::function<QsciScintilla*()>getCurEdit)
{
    NOTEPAD_PLUGIN_IMPLEMENT(HelloWorld);
    // 让插件处在中间位置
    imp->move(pNotepad->geometry().center() - imp->rect().center());

    return 0;
}
